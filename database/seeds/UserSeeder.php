<?php
use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $data = array(
			[
				'name' => 'Jose', 
				'lastname' => 'Hernandez', 
				'email' => 'prueba@gmail.com', 
				'password' => bcrypt(12345678),
				'contactnumber' => '12345678',
			],
			[
				'name' => 'Geek', 
				'lastname' => 'geek', 
				'email' => 'prueba1@gmail.com', 
				'password' => bcrypt(12345678),
				'contactnumber' => '12345678',
			],
			[
				'name' => 'Geek', 
				'lastname' => 'geek', 
				'email' => 'prueba2@gmail.com', 
				'password' => bcrypt(12345678),
				'contactnumber' => '12345678',
			],
			[
				'name' => 'Geek', 
				'lastname' => 'geek', 
				'email' => 'prueba3@gmail.com', 
				'password' => bcrypt(12345678),
				'contactnumber' => '12345678',
			],
			[
				'name' => 'Geek', 
				'lastname' => 'geek', 
				'email' => 'prueba4@gmail.com', 
				'password' => bcrypt(12345678),
				'contactnumber' => '12345678',
			],
			[
				'name' => 'Geek', 
				'lastname' => 'geek', 
				'email' => 'prueba5@gmail.com', 
				'password' => bcrypt(12345678),
				'contactnumber' => '12345678',
			],
			[
				'name' => 'Geek', 
				'lastname' => 'geek', 
				'email' => 'prueba6@gmail.com', 
				'password' => bcrypt(12345678),
				'contactnumber' => '12345678',
			],
			[
				'name' => 'Geek', 
				'lastname' => 'geek', 
				'email' => 'prueba7@gmail.com', 
				'password' => bcrypt(12345678),
				'contactnumber' => '12345678',
			],
			[
				'name' => 'Geek', 
				'lastname' => 'geek', 
				'email' => 'prueba8@gmail.com', 
				'password' => bcrypt(12345678),
				'contactnumber' => '12345678',
			],
			[
				'name' => 'Geek', 
				'lastname' => 'geek', 
				'email' => 'prueba9@gmail.com', 
				'password' => bcrypt(12345678),
				'contactnumber' => '12345678',
			],
			[
				'name' => 'Geek', 
				'lastname' => 'geek', 
				'email' => 'prueba10@gmail.com', 
				'password' => bcrypt(12345678),
				'contactnumber' => '12345678',
			],
			[
				'name' => 'Geek', 
				'lastname' => 'geek', 
				'email' => 'prueba11@gmail.com', 
				'password' => bcrypt(12345678),
				'contactnumber' => '12345678',
			],
			[
				'name' => 'Geek', 
				'lastname' => 'geek', 
				'email' => 'prueba12@gmail.com', 
				'password' => bcrypt(12345678),
				'contactnumber' => '12345678',
			],
			[
				'name' => 'Geek', 
				'lastname' => 'geek', 
				'email' => 'prueba13@gmail.com', 
				'password' => bcrypt(12345678),
				'contactnumber' => '12345678',
			],

			[
				'name' => 'Geek', 
				'lastname' => 'geek', 
				'email' => 'prueba14@gmail.com', 
				'password' => bcrypt(12345678),
				'contactnumber' => '12345678',
			],
			[
				'name' => 'Geek', 
				'lastname' => 'geek', 
				'email' => 'prueba15@gmail.com', 
				'password' => bcrypt(12345678),
				'contactnumber' => '12345678',
			],
			[
				'name' => 'Geek', 
				'lastname' => 'geek', 
				'email' => 'prueba16@gmail.com', 
				'password' => bcrypt(12345678),
				'contactnumber' => '12345678',
			],
			[
				'name' => 'Geek', 
				'lastname' => 'geek', 
				'email' => 'prueba17@gmail.com', 
				'password' => bcrypt(12345678),
				'contactnumber' => '12345678',
			],
			[
				'name' => 'Geek', 
				'lastname' => 'geek', 
				'email' => 'prueba18@gmail.com', 
				'password' => bcrypt(12345678),
				'contactnumber' => '12345678',
			],
			[
				'name' => 'Geek', 
				'lastname' => 'geek', 
				'email' => 'prueba19@gmail.com', 
				'password' => bcrypt(12345678),
				'contactnumber' => '12345678',
			],

			[
				'name' => 'Geek', 
				'lastname' => 'geek', 
				'email' => 'prueba20@gmail.com', 
				'password' => bcrypt(12345678),
				'contactnumber' => '12345678',
			],
			[
				'name' => 'Geek', 
				'lastname' => 'geek', 
				'email' => 'prueba21@gmail.com', 
				'password' => bcrypt(12345678),
				'contactnumber' => '12345678',
			],
			[
				'name' => 'Geek', 
				'lastname' => 'geek', 
				'email' => 'prueba22@gmail.com', 
				'password' => bcrypt(12345678),
				'contactnumber' => '12345678',
			],
			[
				'name' => 'Geek', 
				'lastname' => 'geek', 
				'email' => 'prueba23@gmail.com', 
				'password' => bcrypt(12345678),
				'contactnumber' => '12345678',
			],
			[
				'name' => 'Geek', 
				'lastname' => 'geek', 
				'email' => 'prueba24@gmail.com', 
				'password' => bcrypt(12345678),
				'contactnumber' => '12345678',
			],
			[
				'name' => 'Geek', 
				'lastname' => 'geek', 
				'email' => 'prueba25@gmail.com', 
				'password' => bcrypt(12345678),
				'contactnumber' => '12345678',
			],

			[
				'name' => 'Geek', 
				'lastname' => 'geek', 
				'email' => 'prueba26@gmail.com', 
				'password' => bcrypt(12345678),
				'contactnumber' => '12345678',
			],
			[
				'name' => 'Geek', 
				'lastname' => 'geek', 
				'email' => 'prueba27@gmail.com', 
				'password' => bcrypt(12345678),
				'contactnumber' => '12345678',
			],
			[
				'name' => 'Geek', 
				'lastname' => 'geek', 
				'email' => 'prueba28@gmail.com', 
				'password' => bcrypt(12345678),
				'contactnumber' => '12345678',
			],
			[
				'name' => 'Geek', 
				'lastname' => 'geek', 
				'email' => 'prueba29@gmail.com', 
				'password' => bcrypt(12345678),
				'contactnumber' => '12345678',
			],
			[
				'name' => 'Geek', 
				'lastname' => 'geek', 
				'email' => 'prueba30@gmail.com', 
				'password' => bcrypt(12345678),
				'contactnumber' => '12345678',
			],[
				'name' => 'Geek', 
				'lastname' => 'geek', 
				'email' => 'prueba31@gmail.com', 
				'password' => bcrypt(12345678),
				'contactnumber' => '12345678',
			],

			[
				'name' => 'Geek', 
				'lastname' => 'geek', 
				'email' => 'prueba32@gmail.com', 
				'password' => bcrypt(12345678),
				'contactnumber' => '12345678',
			],

		);
 
		User::insert($data);
 
	}
}
