<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }


    public function home()
    {
        $contacts = User::orderBy('id', 'desc')->paginate(10);
        return view('content.index', compact('contacts'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $contacts = User::orderBy('id', 'desc')->paginate(10);
        return view('content.index', compact('contacts'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create(Request $request)
    {
        if ($request->isMethod('post')) {
            User::create($request->all());
            return redirect()->route('index');

        }else{
            return view('content.create');
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function update(Request $request, $id)
    {
        $contact = User::find($id);
        return view('content.update', compact('contact'));
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function updatesave(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $update = User::find($id);
            $update->name = $request->name;
            $update->lastname = $request->lastname;
            $update->email = $request->email;
            $update->contactnumber = $request->contactnumber;
            $update->password = bcrypt($request->password);
            $update->save();
            return redirect()->route('index');
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function delete(User $id)
    {
        $id->delete();
        return redirect()->route('index');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function search(Request $request)
    {
        $reques = $request->get('search');
        $contacts = User::Contact($reques)->paginate(10);
        return view('content.index', compact('contacts'));
    }


    
}
