<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'lastname', 'email', 'contactnumber', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //scope Search Contact
    public function scopeContact($query, $search)
    {
        if ($search) {
            return $query->where('name', 'like', "%$search%")
                         ->orwhere('lastname', 'like', "%$search%")
                         ->orwhere('email', 'like', "%$search%")
                         ->orwhere('contactnumber', 'like', "%$search%");
        }
    }
}
