@extends('template.template')

@section('content')
<br>
<br>

<div class="container">
<form method="POST" action="{{route('updatesave', $contact->id)}}"> 
  @csrf
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="name">Name</label>
      <input name="name" type="name" class="form-control" value="{{$contact->name}}">
    </div>
    <div class="form-group col-md-6">
      <label for="lastname">Lastname</label>
      <input name="lastname" type="lastname" class="form-control" value="{{$contact->lastname}}">
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="email">Email</label>
      <input name="email" type="email" class="form-control" value="{{$contact->email}}">
    </div>
    <div class="form-group col-md-6">
      <label for="contactnumber">Number Contact</label>
      <input name="contactnumber" type="contactnumber" class="form-control" value="{{$contact->contactnumber}}">
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="password">Password</label>
      <input name="password" type="password" class="form-control" required>
    </div>
  </div>
  <button type="submit" class="btn btn-primary">Edit</button>
</form>
</div>
@endsection