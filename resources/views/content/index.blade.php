@extends('template.template')

@section('content')
<div class="container">
  <br>
  <br>
  <br>
  <div class="row">
    <div class="col-6">
	   <a href="{{ route('create') }}">create</a> 
    </div>
    <div class="col-6">
    <form class="form-inline my-2 my-lg-0" action="{{ route('search') }}" method="post">
      @csrf
      <input class="form-control mr-sm-2" type="search" name="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
    </div>
  </div>
  <br>
  <br>
  <table class="table">
    <caption>List of users</caption>
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Lastname</th>
        <th scope="col">Email</th>
        <th scope="col">Contact Number</th>
        <th scope="col">Edit</th>
        <th scope="col">Delete</th>
      </tr>
    </thead>
    <tbody>
      @foreach($contacts as $key => $contact)
      <tr>
        <th scope="row">{{$key+1}}</th>
        <td>
         {{$contact->name}}
       </td>
       <td>
         {{$contact->lastname}}
       </td>
       <td>
         {{$contact->email}}
       </td>
       <td>
         {{$contact->contactnumber}}
       </td>
       <td>
         <form action="{{ route('update', $contact->id) }}" method="post">
          @csrf
          <button type="submit" >edit</button>
        </form>
      </td>
      <td>
      	<form action="{{ route('delete', $contact->id) }}" method="post">
      		@csrf
         <button  type="submit" >delete</button>
       </td>
     </tr>
    @endforeach
   </tbody>
 </table>
{{ $contacts->links() }}
</div>
@endsection