@extends('template.template')

@section('content')
<br>
<br>
<br>

<div class="container">
  <p>Create Contact</p>
<form method="POST" action="{{ route('create') }}">
  @csrf
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="name">Name</label>
      <input type="text" name="name" class="form-control" required>
    </div>
    <div class="form-group col-md-6">
      <label for="lastname">Last Name</label>
      <input type="text" name="lastname" class="form-control" required>
    </div>
    <div class="form-group col-md-6">
      <label for="email">Email</label>
      <input type="email" name="email" class="form-control" required>
    </div>
    <div class="form-group col-md-6">
      <label for="contactnumber">Contact Number</label>
      <input type="text" name="contactnumber" class="form-control" required>
    </div>
    <div class="form-group col-md-6">
      <label for="password">Password</label>
      <input type="password" name="password" class="form-control" required>
    </div>
  </div>
  <button type="submit" class="btn btn-primary">Create</button>
</form>
</div>
@endsection