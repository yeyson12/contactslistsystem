-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-08-2020 a las 20:38:35
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `contactslistsystem`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contactnumber` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `lastname`, `email`, `email_verified_at`, `password`, `contactnumber`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Jose', 'Hernandez', 'prueba@gmail.com', NULL, '$2y$10$xbrFRmLC5pF5qpC8zgsdW.J95YiHVF854ynazieJSg2r5G.O7lVx2', '12345678', NULL, NULL, NULL),
(2, 'Geek', 'geek', 'prueba1@gmail.com', NULL, '$2y$10$Wg.gC07DQfBViZ.VGgN0ZuX1sNonK72wGcFNsqOvBPXLUNsLMMNW.', '12345678', NULL, NULL, NULL),
(3, 'Geek', 'geek', 'prueba2@gmail.com', NULL, '$2y$10$Bmk/VnvaEYjCqBauqVTE0OtWcKesWjU7t2wKsRpwq83/54EMwXIzO', '12345678', NULL, NULL, NULL),
(4, 'Geek', 'geek', 'prueba3@gmail.com', NULL, '$2y$10$aukFoJnJGIGOJL5hXz6Wi.vfWFwwCYOpWRNpSoyuTyfaKodet.EYa', '12345678', NULL, NULL, NULL),
(5, 'Geek', 'geek', 'prueba4@gmail.com', NULL, '$2y$10$qJdr7mhc3zt2puaEVV1Hiuz8js87MckEdmzmLAYKpimvIYIYEU1uy', '12345678', NULL, NULL, NULL),
(6, 'Geek', 'geek', 'prueba5@gmail.com', NULL, '$2y$10$aJ7mu/talKSo3eOTBS.Hk.uCSOiWd.O3uAh61.ttZn1xLZV2OPZW2', '12345678', NULL, NULL, NULL),
(7, 'Geek', 'geek', 'prueba6@gmail.com', NULL, '$2y$10$6/fraRSgio7OsmPsGpWiBeLPUVXECLQikTWNfJokm6aO69A.SZnBe', '12345678', NULL, NULL, NULL),
(8, 'Geek', 'geek', 'prueba7@gmail.com', NULL, '$2y$10$N3QCd7Z4EnqTVCjzESNSiOOf/HGLQXsXdoKXjsQ8inq9V3GcFBQdW', '12345678', NULL, NULL, NULL),
(9, 'Geek', 'geek', 'prueba8@gmail.com', NULL, '$2y$10$rkSsGXS9llIcCPbKz73fv.7adbE3ukhyT4QFIgJ9RXNEiS/pHhhpK', '12345678', NULL, NULL, NULL),
(10, 'Geek', 'geek', 'prueba9@gmail.com', NULL, '$2y$10$Q1IiGk//T2VaTJF6Ef4sTuaUuRoMU9dSix7wmZfuF.zuY4uJ1U0La', '12345678', NULL, NULL, NULL),
(11, 'Geek', 'geek', 'prueba10@gmail.com', NULL, '$2y$10$VHfVLktyXprO913RL3ZqmORv90EVzBrFlG2mCRb/muqxmtIp49hXe', '12345678', NULL, NULL, NULL),
(12, 'Geek', 'geek', 'prueba11@gmail.com', NULL, '$2y$10$5StIGknlJy/iCta.iyQ/N.JJL3VH.3JEKZUTo.rwqG.cL/jtV5LGq', '12345678', NULL, NULL, NULL),
(13, 'Geek', 'geek', 'prueba12@gmail.com', NULL, '$2y$10$7A2r0rboGnhQYW2.KehA2uP1YwYcXZ3d4C4mz4aIe75Pjz3.nX2ku', '12345678', NULL, NULL, NULL),
(14, 'Geek', 'geek', 'prueba13@gmail.com', NULL, '$2y$10$VJFn2nV3A9TQ2MvHNjEqW.f5nPKkaPGinnJlVhL8XLNWfkPdhwI8m', '12345678', NULL, NULL, NULL),
(15, 'Geek', 'geek', 'prueba14@gmail.com', NULL, '$2y$10$9p63XUJFJonm/m3q31mkj.Vg.ng5lQXNN62CleSXw/biARmwwTzFm', '12345678', NULL, NULL, NULL),
(16, 'Geek', 'geek', 'prueba15@gmail.com', NULL, '$2y$10$ClcsD..YxHHsiY3hoPiyHuQivW/peh6UcyFeE9srh0N.pmF8GD9oS', '12345678', NULL, NULL, NULL),
(17, 'Geek', 'geek', 'prueba16@gmail.com', NULL, '$2y$10$jWsSFXgSFe21SWlpI.r9geL9Q5sZtEBNkiy4CZM7itirtV4Sb5jvC', '12345678', NULL, NULL, NULL),
(18, 'Geek', 'geek', 'prueba17@gmail.com', NULL, '$2y$10$RkVBOMBQpjLhl3rbUy5CteKMjnjS3rMDmj1v1hGI5s9IEge0kc.xK', '12345678', NULL, NULL, NULL),
(19, 'Geek', 'geek', 'prueba18@gmail.com', NULL, '$2y$10$v05soAZCeoa9wCb1vlFX.upKlDhWYMGUntSomljqre/hZEXI0u7hW', '12345678', NULL, NULL, NULL),
(20, 'Geek', 'geek', 'prueba19@gmail.com', NULL, '$2y$10$QxJW/svo7on0lLgyfMSb5eHy3ixXq7Y5r49xj6oDm6GSWPFUeFYFC', '12345678', NULL, NULL, NULL),
(21, 'Geek', 'geek', 'prueba20@gmail.com', NULL, '$2y$10$0IiB74qGRtkYA.UF6e827egYr68ESeooupdE34T8OiHF2Y2orgF2m', '12345678', NULL, NULL, NULL),
(22, 'Geek', 'geek', 'prueba21@gmail.com', NULL, '$2y$10$m2VwSoNn8gMETzmBEqgKteYv/wYI8Zlihn7xeDlp9U80q2nPABXRG', '12345678', NULL, NULL, NULL),
(23, 'Geek', 'geek', 'prueba22@gmail.com', NULL, '$2y$10$PsZVhhQ2w8gAToN/lqJLjOl8bccs2L6CcOSRPjkpQywMBkXignZWa', '12345678', NULL, NULL, NULL),
(24, 'Geek', 'geek', 'prueba23@gmail.com', NULL, '$2y$10$pSm6YyYSmMpKAZA4Ost2fOGpplJB5f2Iu.SUXg2tHAEre6psaUfzW', '12345678', NULL, NULL, NULL),
(26, 'Geek', 'geek', 'prueba25@gmail.com', NULL, '$2y$10$HP6eOKDhBK9wjvqeP4JGBuapsHX3fCSa9U0iY3o5nMau5v5AgukNq', '12345678', NULL, NULL, NULL),
(27, 'Geek', 'geek', 'prueba26@gmail.com', NULL, '$2y$10$QzQoYjw3GFvRaoKyu6mgnuvSe4fOw.JghC8daGDRKy59bh9XQ3jjC', '12345678', NULL, NULL, NULL),
(28, 'Geek', 'geek', 'prueba27@gmail.com', NULL, '$2y$10$DoDIXJ6xzsFAH.qM5jQ/PeMZNtbb3ZXhmC6PX7XhdVq0H2COOmv6C', '12345678', NULL, NULL, NULL),
(29, 'Geek', 'geek', 'prueba28@gmail.com', NULL, '$2y$10$YQKVt4o.Qxwc3lDGSIlNcOR0TTFQt2.tFCAOSE5VjIa9z/an6q1ky', '12345678', NULL, NULL, NULL),
(30, 'Geek', 'geek', 'prueba29@gmail.com', NULL, '$2y$10$YsLr0SUZMiY7unbjDzvCUeKJ7TgLn0016o7Kq4pCxDOCPthOMDz82', '12345678', NULL, NULL, NULL),
(31, 'Geek', 'geek', 'prueba30@gmail.com', NULL, '$2y$10$zsTxLl/fVJ0M4Hx8gqy5HOP085Kf3A.Ea5hktO5PSH6npkL2QFps.', '12345678', NULL, NULL, NULL),
(32, 'Geek', 'geek', 'prueba31@gmail.com', NULL, '$2y$10$Q.TqaiHahf4FQoO7NJLaR.XlJmPAge7iwp1MpoAbctL3vi/FhDgVe', '12345678', NULL, NULL, NULL),
(33, 'Geek', 'geek', 'prueba32@gmail.com', NULL, '$2y$10$St5kpnanOjJs0p8mbgCf9eSfl8QfEUmQFefZnPaVcjFetVmou6EmS', '12345678', NULL, NULL, NULL),
(34, 'jose', 'alberto', 'prioridad.25@gmail.com', NULL, '12345678', '11111111111111', NULL, '2020-08-25 21:50:16', '2020-08-25 21:50:16'),
(36, 'jefe', 'pe', 'prioridad@gmail.com', NULL, '12345678', '1111111', NULL, '2020-08-25 21:52:05', '2020-08-25 21:52:05'),
(37, 'prueba', 'albert', 'prioridadrgsfgsdfg@gmail.com', NULL, '$2y$10$Fx4skpU3VaLd1wKZwBuQtOzbg5PRd6DbOe3NEQZmFJN8ly7m9wE1S', '1234456', NULL, '2020-08-25 22:33:08', '2020-08-25 22:33:08');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
