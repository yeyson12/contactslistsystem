<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('home', 'HomeController@home')->name('home');
Auth::routes();
Route::get('/', 'HomeController@index')->name('index');
Route::match(['get', 'post'],'/create', 'HomeController@create')->name('create');
Route::post('/update/{id}', 'HomeController@update')->name('update');
Route::post('/updatesave/{id}', 'HomeController@updatesave')->name('updatesave');
Route::post('delete/{id}', 'HomeController@delete')->name('delete');
Route::post('search', 'HomeController@search')->name('search');




